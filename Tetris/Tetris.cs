﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using NAudio;
using NAudio.Wave;
using Tetris.Properties;
using System.IO;

namespace Tetris
{
	public partial class Tetris : Form
	{
		Brush[] drawColors;
		int[,] playField, fieldCurBlock, brickTile, emptyTile, lgrayTile;
		int[][,] tileTypes, bricks0, bricks90, bricks180, bricks270;
		int[,] OTile, ITile01, ITile02, ITile03, LTile, JTile, ZTile, STile, TTile, ITile901, ITile902, ITile903;
		int[,] OBrick0, IBrick0, LBrick0, JBrick0, ZBrick0, SBrick0, TBrick0;
		int[,] Char0, Char1, Char2, Char3, Char4, Char5, Char6, Char7, Char8, Char9,
			   CharA, CharB, CharC, CharD, CharE, CharF, CharG, CharH, CharI, CharJ,
			   CharK, CharL, CharM, CharN, CharO, CharP, CharQ, CharR, CharS, CharT,
			   CharU, CharV, CharW, CharX, CharY, CharZ, CharPer, CharDash, CharMul, CharHeart;

		Dictionary<char, int[,]> charset = new Dictionary<char, int[,]>();

		public bool gameOver = false, wait = false, gameStartup = true, isBlockTurnKey = false, isLeftRightKey = false,
			softDropping = false;
		
		int score = 0, startLevel = 0, level = 0, linesCleared = 0, gameOverThreshold = 0, nextBlock,
			xMove = 0, yMove = 0;
		int[] speedLevels = new int[21]
			{ 887, 820, 753, 686, 619, 552, 469, 368, 285, 184, 167, 151, 134, 117, 100, 100, 84, 84, 67, 67, 50 };

		Random nextBlockNum = new Random();

		SoundContainer sCont = new SoundContainer();

		Thread md, ml, mr;

		public Tetris()
		{
			InitializeComponent();

			playField = new int[21, 16];
			fieldCurBlock = new int[21, 16];
			for (int x = 0; x < 16; x++)
			{
				playField[18, x] = -1;
			}
			for (int y = 0; y < 18; y++)
			{
				playField[y, 2] = -1;
				playField[y, 13] = -1;
			}

			sCont.PlayMusic(0);

			InitTiles();
			tileTypes = new int[14][,]
				{ emptyTile, OTile, ITile01, ITile02, ITile03, LTile, JTile, ZTile, STile, TTile, ITile901, ITile902, ITile903, lgrayTile };

			#region charset
			InitCharset();
			charset.Add('0', Char0); charset.Add('1', Char1); charset.Add('2', Char2); charset.Add('3', Char3); charset.Add('4', Char4);
			charset.Add('5', Char5); charset.Add('6', Char6); charset.Add('7', Char7); charset.Add('8', Char8); charset.Add('9', Char9);
			charset.Add('A', CharA); charset.Add('B', CharB); charset.Add('C', CharC); charset.Add('D', CharD); charset.Add('E', CharE);
			charset.Add('F', CharF); charset.Add('G', CharG); charset.Add('H', CharH); charset.Add('I', CharI); charset.Add('J', CharJ);
			charset.Add('K', CharK); charset.Add('L', CharL); charset.Add('M', CharM); charset.Add('N', CharN); charset.Add('O', CharO);
			charset.Add('P', CharP); charset.Add('Q', CharQ); charset.Add('R', CharR); charset.Add('S', CharS); charset.Add('T', CharT);
			charset.Add('U', CharU); charset.Add('V', CharV); charset.Add('W', CharW); charset.Add('X', CharX); charset.Add('Y', CharY);
			charset.Add('Z', CharZ); charset.Add('.', CharPer); charset.Add('-', CharDash); charset.Add('*', CharMul); charset.Add('<', CharHeart);
			charset.Add(' ', emptyTile);
			#endregion

			InitBricks();
			bricks0 = new int[7][,]
				{ LBrick0  , JBrick0  , IBrick0 , OBrick0, ZBrick0 , SBrick0 , TBrick0   };
			bricks90 = new int[7][,]
				{ LBrick90 , JBrick90 , IBrick90, OBrick0, ZBrick90, SBrick90, TBrick90  };
			bricks180 = new int[7][,]
				{ LBrick180, JBrick180, IBrick0 , OBrick0, ZBrick0 , SBrick0 , TBrick180 };
			bricks270 = new int[7][,]
				{ LBrick270, JBrick270, IBrick90, OBrick0, ZBrick90, SBrick90, TBrick270 };

			drawColors = new Brush[4]
				{ Brushes.Black, Brushes.Gray, Brushes.LightGray, Brushes.White };

			nextBlock = nextBlockNum.Next(0, 7);
			GetNextBlock();
			level = startLevel;
			BlockDescTimer.Interval = speedLevels[level];
			BlockDescTimer.Start();

			md = new Thread(MoveDown);
			ml = new Thread(MoveLeft);
			mr = new Thread(MoveRight);
		}

		private void Tetris_KeyDown(object sender, KeyEventArgs e)
		{
			string lastMove = "";

			if (!gameOver && !wait)
			{
				switch (e.KeyCode)
				{
					case Keys.A:
						//blockOffsetX--;
						xMove = -1; yMove = 0;
						mr.Abort(); md.Abort();
						if (!ml.IsAlive) { ml = new Thread(MoveLeft); ml.Start(); }
						lastMove = "x-";
						isBlockTurnKey = false;
                        isLeftRightKey = true;
						break;
					case Keys.D:
						//blockOffsetX++;
						xMove = 1; yMove = 0;
						ml.Abort(); md.Abort();
						if (!mr.IsAlive) { mr = new Thread(MoveRight); mr.Start(); }
						lastMove = "x+";
						isBlockTurnKey = false;
                        isLeftRightKey = true;
						break;
					case Keys.S:
						//blockOffsetY++;
						yMove = 1; xMove = 0;
						ml.Abort(); mr.Abort();
						if (!md.IsAlive) { md = new Thread(MoveDown); md.Start(); }
						//BlockDescTimer.Interval = 50;
						lastMove = "y+";
						softDropping = true;
						isBlockTurnKey = false;
                        isLeftRightKey = false;
						break;
					case Keys.J:
						xMove = yMove = 0;
						blockOrientation = (blockOrientation + 1) % 4;
						lastMove = "rr";
						isBlockTurnKey = true;
                        isLeftRightKey = false;
						break;
					case Keys.K:
						xMove = yMove = 0;
						blockOrientation = (blockOrientation - 1) % 4;
						lastMove = "rl";
						isBlockTurnKey = true;
                        isLeftRightKey = false;
                        break;
					default: // Fixes issue #2
						xMove = yMove = 0;
						isBlockTurnKey = false;
                        isLeftRightKey = false;
                        break;                  
				}
				DrawCurBlock();
				UndoInvalidMoves(lastMove);
			}

			Invoke(new Action(UpdateForm));
		}

		private void Tetris_KeyUp(object sender, KeyEventArgs e)
		{
			softDropping = false;
			BlockDescTimer.Interval = speedLevels[level];
			xMove = yMove = 0;

			ml.Abort(); mr.Abort(); md.Abort();
		}

		private void UndoInvalidMoves(string move)
		{
			if (ColCheck())
			{
				switch (move)
				{
					case "x-":
						blockOffsetX++;
						DrawCurBlock();
						break;
					case "x+":
						blockOffsetX--;
						DrawCurBlock();
						break;
					case "y+":
						blockOffsetY--;
						DrawCurBlock();
						GetNextBlock();
						break;
					case "rr":
						blockOrientation = (blockOrientation - 1) % 4;
						if (blockOrientation < 0) blockOrientation += 4;
						DrawCurBlock();
						break;
					case "rl":
						blockOrientation = (blockOrientation + 1) % 4;
						if (blockOrientation > 270) blockOrientation -= 4;
						DrawCurBlock();
						break;
				}
			}
			else
			{
				gameOverThreshold = 0;
				if (isBlockTurnKey) sCont.PlaySound(0);
				if (isLeftRightKey) sCont.PlaySound(3);
			}
		}

		private void BlockDescTimer_Tick(object sender, EventArgs e)
		{
			blockOffsetY++;
			ChangeBlockPos("y+");

			this.Invalidate();
			this.Refresh();
		}

		public void MoveDown() {
			while (yMove != 0)
			{
				blockOffsetY++;
				Invoke(new Action(DrawCurBlock));
				UndoInvalidMoves("y+");
				Invoke(new Action(UpdateForm));
				Thread.Sleep(50);
			}
		}
		public void MoveLeft()
		{
			// These following lines that are commented out don't work properly...
			// I think that the first time you attempt to invoke UpdateForm, it doesn't actually work
			// Whyever the fuck that may be...

			/*blockOffsetX--;
			Invoke(new Action(UpdateForm));
			Thread.Sleep(200);*/
			while (xMove != 0)
			{
				blockOffsetX--;
				Invoke(new Action(DrawCurBlock));
				UndoInvalidMoves("x-");
				Invoke(new Action(UpdateForm));
				Thread.Sleep(100);
			}
		}
		public void MoveRight()
		{
			/*blockOffsetX++;
			Invoke(new Action(UpdateForm));
			Thread.Sleep(200);*/
			while (xMove != 0)
			{
				blockOffsetX++;
				Invoke(new Action(DrawCurBlock));
				UndoInvalidMoves("x+");
				Invoke(new Action(UpdateForm));
				Thread.Sleep(100);
			}
		}

		public void UpdateForm()
		{
			Refresh();
			Invalidate();
		}

		int[,] IBrick90, LBrick90, JBrick90, ZBrick90, SBrick90, TBrick90;
		int[,] LBrick180, JBrick180, TBrick180;
		int[,] LBrick270, JBrick270, TBrick270;

		int  curBlock = 0,
			 blockOffsetX = 0,
			 blockOffsetY = 0;
		uint blockOrientation = 0;

		public void ChangeBlockPos(string lastMove)
		{
			DrawCurBlock();

			if (ColCheck() && lastMove == "y+")
			{
				blockOffsetY--;
				if (blockOffsetY < 0)
				{
					blockOffsetY = 0;
					if (gameOverThreshold >= 1)
					{
						BlockDescTimer.Enabled = false;
						gameOver = true;
						sCont.StopMusic();
					}
					else gameOverThreshold++;
				}
				else
				{
					DrawCurBlock();
					GetNextBlock();
				}
			}
		}

		public void DrawCurBlock()
		{
			for (int x = 0; x < 16; x++)
				for (int y = 0; y < 20; y++)
					fieldCurBlock[y, x] = 0;

			if (blockOffsetY < 0) blockOffsetY = 0;

			for (int x = 0; x < 4; x++)
			{
				for (int y = 0; y < 4; y++)
				{
					switch (blockOrientation)
					{
						case 0:
							fieldCurBlock[y + blockOffsetY, x + 6 + blockOffsetX] = bricks0[curBlock][y, x];
							break;
						case 1:
							fieldCurBlock[y + blockOffsetY, x + 6 + blockOffsetX] = bricks90[curBlock][y, x];
							break;
						case 2:
							fieldCurBlock[y + blockOffsetY, x + 6 + blockOffsetX] = bricks180[curBlock][y, x];
							break;
						case 3:
							fieldCurBlock[y + blockOffsetY, x + 6 + blockOffsetX] = bricks270[curBlock][y, x];
							break;
					}
				}
			}
		}

		public bool ColCheck()
		{
			for (int x = 0; x < 16; x++)
			{
				for (int y = 0; y < 21; y++)
				{
					if (playField[y, x] != 0 && fieldCurBlock[y, x] != 0)
						return true;
				}
			}

			return false;
		}

		public void GetNextBlock()
		{
			if (!gameStartup) sCont.PlaySound(1);
			else gameStartup = false;
			curBlock = nextBlock;
			blockOffsetX = 0;
			blockOffsetY = 0;
			blockOrientation = 0;
			BlockDescTimer.Interval = 887;

			//BlockDescTimer.Stop(); Thread.Sleep(30); BlockDescTimer.Start();

			for (int x = 0; x < 16; x++)
			{
				for (int y = 0; y < 20; y++)
				{
					playField[y, x] |= fieldCurBlock[y, x];
				}
			}

			CheckFullLines();
			ChangeBlockPos("");
		}

		public void CheckFullLines()
		{
			List<int> fullLines = new List<int>();

			for (int y = 0; y < 18; y++)
			{
				bool isFull = true;

				for (int x = 0; x < 10; x++)
				{
					if (playField[y, x + 3] == 0)
					{
						isFull = false;
						break;
					}
				}

				if (isFull) fullLines.Add(y);
			}

			if (fullLines.Count > 0) ClearLines(fullLines);

			nextBlock = nextBlockNum.Next(0, 7);
		}

		// With the new way of block movement, this SOMETIMES causes an exception, unsure as to why.
		public void ClearLines(List<int> lines)
		{
			int[] tempLineStor = new int[10];

			sCont.PlaySound(2);
			#region Animation stuff
			foreach (int i in lines)
			{
				for (int x = 0; x < 10; x++)
				{
					fieldCurBlock[i, x + 3] = 13;
				}
			}
			Invalidate(); Refresh(); Thread.Sleep(165);
			foreach (int i in lines)
			{
				for (int x = 0; x < 10; x++)
				{
					fieldCurBlock[i, x + 3] = 0;
				}
			}
			Invalidate(); Refresh(); Thread.Sleep(165);
			foreach (int i in lines)
			{
				for (int x = 0; x < 10; x++)
				{
					fieldCurBlock[i, x + 3] = 13;
				}
			}
			Invalidate(); Refresh(); Thread.Sleep(165);
			foreach (int i in lines)
			{
				for (int x = 0; x < 10; x++)
				{
					fieldCurBlock[i, x + 3] = 0;
				}
			}
			Invalidate(); Refresh(); Thread.Sleep(165);
			foreach (int i in lines)
			{
				for (int x = 0; x < 10; x++)
				{
					fieldCurBlock[i, x + 3] = 13;
				}
			}
			Invalidate(); Refresh(); Thread.Sleep(165);
			foreach (int i in lines)
			{
				for (int x = 0; x < 10; x++)
				{
					fieldCurBlock[i, x + 3] = 0;
				}
			}
			Invalidate(); Refresh(); Thread.Sleep(165);
			foreach (int i in lines)
			{
				for (int x = 0; x < 10; x++)
				{
					playField[i, x + 3] = 0;
				}
			}
			Invalidate(); Refresh(); Thread.Sleep(250);
			foreach (int i in lines)
			{
				for (int y = i; y > 0; y--)
				{
					for (int x = 0; x < 10; x++)
					{
						fieldCurBlock[y, x + 3] = 0;
						playField[y, x + 3] = playField[y - 1, x + 3];
					}
				}
			}
			#endregion
			sCont.PlaySound(1);
			linesCleared += lines.Count();
			switch (lines.Count)
			{
				case 1:
					score += (40 * (level + 1));
					break;
				case 2:
					score += (100 * (level + 1));
					break;
				case 3:
					score += (300 * (level + 1));
					break;
				case 4:
					score += (1200 * (level + 1));
					break;
				default: break;
			}
			level = startLevel + (linesCleared / 10);
			BlockDescTimer.Interval = speedLevels[level];
			Invalidate(); Refresh(); Thread.Sleep(30);
		}

		public void DrawTile(Graphics g, int[,] tile, int tWidth, int tHeight, int xPos, int yPos)
		{
			for (int y = 0; y < tHeight; y++)
			{
				for (int x = 0; x < tWidth; x++)
				{
					if (tile != emptyTile)
						g.FillRectangle(drawColors[tile[y, x]], new Rectangle(xPos + 2 * x, yPos + 2 * y, 2, 2));
				}
			}
		}

		public void PutText(Graphics g, string text, int tPosX, int tPosY)
		{
			for (int i = 0; i < text.Length; i++)
				DrawTile(g, charset[text[i]], 8, 8, 16 * (tPosX + i), 16 * tPosY);
		}

		private void Tetris_Paint(object sender, PaintEventArgs e)
		{
			#region paint UI stuff
			e.Graphics.FillRectangle(drawColors[0], new Rectangle(0, 0, 16, 288));
			e.Graphics.FillRectangle(drawColors[0], new Rectangle(210, 0, 110, 288));

			e.Graphics.FillRectangle(drawColors[3], new Rectangle(210, 26, 110, 44));
			e.Graphics.FillRectangle(drawColors[1], new Rectangle(210, 28, 110, 14));
			e.Graphics.FillRectangle(drawColors[1], new Rectangle(210, 44, 110, 2));
			e.Graphics.FillRectangle(drawColors[1], new Rectangle(210, 66, 110, 2));

			e.Graphics.FillRectangle(drawColors[3], new Rectangle(218, 12, 92, 24));
			e.Graphics.FillRectangle(drawColors[3], new Rectangle(220, 10, 88, 28));
			e.Graphics.FillRectangle(drawColors[1], new Rectangle(220, 14, 88, 20));
			e.Graphics.FillRectangle(drawColors[1], new Rectangle(222, 12, 84, 24));
			e.Graphics.FillRectangle(drawColors[3], new Rectangle(222, 16, 84, 16));
			e.Graphics.FillRectangle(drawColors[3], new Rectangle(224, 14, 80, 20));
			PutText(e.Graphics, "SCORE", 14, 1);
			PutText(e.Graphics, score.ToString().PadLeft(6, ' '), 13, 3);

			e.Graphics.FillRectangle(drawColors[3], new Rectangle(218, 92, 92, 40));
			e.Graphics.FillRectangle(drawColors[3], new Rectangle(220, 90, 88, 44));
			e.Graphics.FillRectangle(drawColors[1], new Rectangle(220, 94, 88, 36));
			e.Graphics.FillRectangle(drawColors[1], new Rectangle(222, 92, 84, 40));
			e.Graphics.FillRectangle(drawColors[3], new Rectangle(222, 96, 84, 32));
			e.Graphics.FillRectangle(drawColors[3], new Rectangle(224, 94, 80, 36));
			PutText(e.Graphics, "LEVEL", 14, 6);
			PutText(e.Graphics, level.ToString().PadLeft(2, ' '), 16, 7);

			e.Graphics.FillRectangle(drawColors[3], new Rectangle(218, 140, 92, 40));
			e.Graphics.FillRectangle(drawColors[3], new Rectangle(220, 138, 88, 44));
			e.Graphics.FillRectangle(drawColors[1], new Rectangle(220, 142, 88, 36));
			e.Graphics.FillRectangle(drawColors[1], new Rectangle(222, 140, 84, 40));
			e.Graphics.FillRectangle(drawColors[3], new Rectangle(222, 144, 84, 32));
			e.Graphics.FillRectangle(drawColors[3], new Rectangle(224, 142, 80, 36));
			PutText(e.Graphics, "LINES", 14, 9);
			PutText(e.Graphics, linesCleared.ToString().PadLeft(2, ' '), 16, 10);

			e.Graphics.FillRectangle(drawColors[2], new Rectangle(230, 202, 84, 76));
			e.Graphics.FillRectangle(drawColors[2], new Rectangle(232, 200, 80, 80));
			e.Graphics.FillRectangle(drawColors[2], new Rectangle(234, 198, 76, 84));
			e.Graphics.FillRectangle(drawColors[3], new Rectangle(232, 202, 80, 76));
			e.Graphics.FillRectangle(drawColors[3], new Rectangle(234, 200, 76, 80));
			e.Graphics.FillRectangle(drawColors[2], new Rectangle(234, 204, 76, 72));
			e.Graphics.FillRectangle(drawColors[2], new Rectangle(236, 202, 72, 76));
			e.Graphics.FillRectangle(drawColors[1], new Rectangle(234, 206, 76, 68));
			e.Graphics.FillRectangle(drawColors[1], new Rectangle(236, 204, 72, 72));
			e.Graphics.FillRectangle(drawColors[1], new Rectangle(238, 202, 68, 76));
			e.Graphics.FillRectangle(drawColors[0], new Rectangle(236, 206, 72, 68));
			e.Graphics.FillRectangle(drawColors[0], new Rectangle(238, 204, 68, 72));
			e.Graphics.FillRectangle(drawColors[3], new Rectangle(238, 206, 68, 68));
			for (int x = 0; x < 4; x++)
			{
				for (int y = 0; y < 4; y++)
				{
					DrawTile(e.Graphics, tileTypes[bricks0[nextBlock][y, x]], 8, 8, 16 * (15 + x), 16 * (13 + y));
				}
			}

			for (int y = 0; y < 288; y += 12)
				DrawTile(e.Graphics, brickTile, 8, 6, 16, y);
			for (int y = 0; y < 288; y += 12)
				DrawTile(e.Graphics, brickTile, 8, 6, 192, y);
			#endregion

			for (int y = 0; y < 18; y++)
				for (int x = 0; x < 10; x++)
					// The '% 14' here basically makes sure that the index is never larger than 13,
					// so that there is always a match in the tileTypes array.
					DrawTile(e.Graphics, tileTypes[playField[y, x + 3] % 14], 8, 8, 32 + 16 * x, 16 * y);
			for (int y = 0; y < 18; y++)
				for (int x = 0; x < 10; x++)
					DrawTile(e.Graphics, tileTypes[fieldCurBlock[y, x + 3]], 8, 8, 32 + 16 * x, 16 * y);
		}

		public void InitBricks()
		{
			#region bricks
			OBrick0 = new int[4, 4]
			{
				{ 0, 0, 0, 0 },
				{ 0, 1, 1, 0 },
				{ 0, 1, 1, 0 },
				{ 0, 0, 0, 0 }
			};

			IBrick0 = new int[4, 4]
			{
				{ 0, 0, 0, 0 },
				{10,11,11,12 },
				{ 0, 0, 0, 0 },
				{ 0, 0, 0, 0 }
			};

			LBrick0 = new int[4, 4]
			{
				{ 0, 0, 0, 0 },
				{ 5, 5, 5, 0 },
				{ 5, 0, 0, 0 },
				{ 0, 0, 0, 0 }
			};

			JBrick0 = new int[4, 4]
			{
				{ 0, 0, 0, 0 },
				{ 6, 6, 6, 0 },
				{ 0, 0, 6, 0 },
				{ 0, 0, 0, 0 }
			};

			ZBrick0 = new int[4, 4]
			{
				{ 0, 0, 0, 0 },
				{ 7, 7, 0, 0 },
				{ 0, 7, 7, 0 },
				{ 0, 0, 0, 0 }
			};

			SBrick0 = new int[4, 4]
			{
				{ 0, 0, 0, 0 },
				{ 0, 8, 8, 0 },
				{ 8, 8, 0, 0 },
				{ 0, 0, 0, 0 }
			};

			TBrick0 = new int[4, 4]
			{
				{ 0, 0, 0, 0 },
				{ 9, 9, 9, 0 },
				{ 0, 9, 0, 0 },
				{ 0, 0, 0, 0 }
			};


			IBrick90 = new int[4, 4]
			{
				{ 0, 2, 0, 0 },
				{ 0, 3, 0, 0 },
				{ 0, 3, 0, 0 },
				{ 0, 4, 0, 0 }
			};

			LBrick90 = new int[4, 4]
			{
				{ 5, 5, 0, 0 },
				{ 0, 5, 0, 0 },
				{ 0, 5, 0, 0 },
				{ 0, 0, 0, 0 }
			};

			JBrick90 = new int[4, 4]
			{
				{ 0, 6, 0, 0 },
				{ 0, 6, 0, 0 },
				{ 6, 6, 0, 0 },
				{ 0, 0, 0, 0 }
			};

			ZBrick90 = new int[4, 4]
			{
				{ 0, 7, 0, 0 },
				{ 7, 7, 0, 0 },
				{ 7, 0, 0, 0 },
				{ 0, 0, 0, 0 }
			};

			SBrick90 = new int[4, 4]
			{
				{ 8, 0, 0, 0 },
				{ 8, 8, 0, 0 },
				{ 0, 8, 0, 0 },
				{ 0, 0, 0, 0 }
			};

			TBrick90 = new int[4, 4]
			{
				{ 0, 9, 0, 0 },
				{ 9, 9, 0, 0 },
				{ 0, 9, 0, 0 },
				{ 0, 0, 0, 0 }
			};


			LBrick180 = new int[4, 4]
			{
				{ 0, 0, 5, 0 },
				{ 5, 5, 5, 0 },
				{ 0, 0, 0, 0 },
				{ 0, 0, 0, 0 }
			};

			JBrick180 = new int[4, 4]
			{
				{ 6, 0, 0, 0 },
				{ 6, 6, 6, 0 },
				{ 0, 0, 0, 0 },
				{ 0, 0, 0, 0 }
			};

			TBrick180 = new int[4, 4]
			{
				{ 0, 9, 0, 0 },
				{ 9, 9, 9, 0 },
				{ 0, 0, 0, 0 },
				{ 0, 0, 0, 0 }
			};


			LBrick270 = new int[4, 4]
			{
				{ 0, 5, 0, 0 },
				{ 0, 5, 0, 0 },
				{ 0, 5, 5, 0 },
				{ 0, 0, 0, 0 }
			};

			JBrick270 = new int[4, 4]
			{
				{ 0, 6, 6, 0 },
				{ 0, 6, 0, 0 },
				{ 0, 6, 0, 0 },
				{ 0, 0, 0, 0 }
			};

			TBrick270 = new int[4, 4]
			{
				{ 0, 9, 0, 0 },
				{ 0, 9, 9, 0 },
				{ 0, 9, 0, 0 },
				{ 0, 0, 0, 0 }
			};
			#endregion
		}

		public void InitTiles()
		{
			#region brick tileset
			brickTile = new int[6, 8]
			{
				{ 2, 0, 3, 2, 2, 0, 3, 2 },
				{ 2, 0, 2, 2, 2, 0, 2, 2 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 3, 2, 2, 0, 3, 2, 2, 0 },
				{ 2, 2, 2, 0, 2, 2, 2, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 }
			};

			emptyTile = new int[8, 8];
			for (int x = 0; x < 8; x++)
				for (int y = 0; y < 8; y++)
					emptyTile[y, x] = 3;

			lgrayTile = new int[8, 8];
			for (int x = 0; x < 8; x++)
				for (int y = 0; y < 8; y++)
					lgrayTile[y, x] = 2;



			OTile = new int[8, 8]
			{
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 3, 3, 3, 3, 3, 3, 0 },
				{ 0, 3, 0, 0, 0, 0, 3, 0 },
				{ 0, 3, 0, 0, 0, 0, 3, 0 },
				{ 0, 3, 0, 0, 0, 0, 3, 0 },
				{ 0, 3, 0, 0, 0, 0, 3, 0 },
				{ 0, 3, 3, 3, 3, 3, 3, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 }
			};

			ITile01 = new int[8, 8]
			{
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 2, 2, 2, 1, 2, 2, 0 },
				{ 0, 2, 1, 2, 2, 2, 1, 0 },
				{ 0, 2, 2, 2, 2, 2, 2, 0 },
				{ 0, 1, 2, 2, 1, 2, 2, 0 },
				{ 0, 2, 2, 2, 2, 2, 1, 0 },
				{ 0, 2, 1, 2, 1, 2, 2, 0 },
				{ 0, 2, 2, 2, 2, 2, 2, 0 }
			};

			ITile02 = new int[8, 8]
			{
				{ 0, 2, 2, 1, 2, 2, 1, 0 },
				{ 0, 1, 2, 2, 2, 2, 2, 0 },
				{ 0, 2, 2, 2, 1, 2, 1, 0 },
				{ 0, 2, 1, 2, 2, 2, 2, 0 },
				{ 0, 2, 2, 2, 2, 2, 1, 0 },
				{ 0, 1, 2, 1, 2, 2, 2, 0 },
				{ 0, 2, 2, 2, 2, 1, 2, 0 },
				{ 0, 2, 1, 2, 2, 2, 2, 0 }
			};

			ITile03 = new int[8, 8]
			{
				{ 0, 2, 2, 2, 2, 2, 1, 0 },
				{ 0, 2, 2, 1, 2, 2, 2, 0 },
				{ 0, 1, 2, 2, 2, 1, 2, 0 },
				{ 0, 2, 2, 1, 2, 2, 2, 0 },
				{ 0, 1, 2, 2, 2, 2, 1, 0 },
				{ 0, 2, 2, 2, 1, 2, 2, 0 },
				{ 0, 2, 1, 2, 2, 2, 2, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 }
			};

			ITile901 = new int[8, 8]
			{
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 2, 2, 2, 1, 2, 2, 2 },
				{ 0, 2, 1, 2, 2, 2, 1, 2 },
				{ 0, 2, 2, 2, 2, 2, 2, 2 },
				{ 0, 1, 2, 2, 1, 2, 1, 2 },
				{ 0, 2, 2, 2, 2, 2, 2, 2 },
				{ 0, 2, 1, 2, 2, 1, 2, 2 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 }
			};

			ITile902 = new int[8, 8]
			{
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 2, 1, 2, 2, 2, 1, 2, 2 },
				{ 2, 2, 2, 1, 2, 2, 2, 1 },
				{ 1, 2, 2, 2, 2, 1, 2, 2 },
				{ 2, 2, 1, 2, 2, 2, 2, 2 },
				{ 2, 2, 2, 2, 2, 2, 1, 2 },
				{ 1, 2, 1, 2, 1, 2, 2, 2 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 }
			};

			ITile903 = new int[8, 8]
			{
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 2, 2, 1, 2, 1, 2, 1, 0 },
				{ 2, 2, 2, 2, 2, 2, 1, 0 },
				{ 2, 1, 2, 1, 2, 2, 2, 0 },
				{ 2, 2, 2, 2, 2, 1, 2, 0 },
				{ 2, 2, 1, 2, 2, 2, 2, 0 },
				{ 1, 2, 2, 2, 1, 2, 2, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 }
			};

			LTile = new int[8, 8]
			{
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 1, 1, 1, 1, 1, 1, 0 },
				{ 0, 1, 1, 1, 1, 1, 1, 0 },
				{ 0, 1, 1, 1, 1, 1, 1, 0 },
				{ 0, 1, 1, 1, 1, 1, 1, 0 },
				{ 0, 1, 1, 1, 1, 1, 1, 0 },
				{ 0, 1, 1, 1, 1, 1, 1, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 }
			};

			JTile = new int[8, 8]
			{
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 2, 2, 2, 2, 2, 2, 0 },
				{ 0, 2, 0, 0, 0, 0, 2, 0 },
				{ 0, 2, 0, 3, 3, 0, 2, 0 },
				{ 0, 2, 0, 3, 3, 0, 2, 0 },
				{ 0, 2, 0, 0, 0, 0, 2, 0 },
				{ 0, 2, 2, 2, 2, 2, 2, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 }
			};

			ZTile = new int[8, 8]
			{
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 2, 2, 2, 2, 2, 2, 0 },
				{ 0, 2, 2, 2, 2, 2, 2, 0 },
				{ 0, 2, 2, 0, 0, 2, 2, 0 },
				{ 0, 2, 2, 0, 0, 2, 2, 0 },
				{ 0, 2, 2, 2, 2, 2, 2, 0 },
				{ 0, 2, 2, 2, 2, 2, 2, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 }
			};

			STile = new int[8, 8]
			{
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 1, 1, 1, 1, 1, 1, 0 },
				{ 0, 1, 0, 0, 0, 0, 1, 0 },
				{ 0, 1, 0, 3, 3, 0, 1, 0 },
				{ 0, 1, 0, 3, 3, 0, 1, 0 },
				{ 0, 1, 0, 0, 0, 0, 1, 0 },
				{ 0, 1, 1, 1, 1, 1, 1, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 }
			};

			TTile = new int[8, 8]
			{
				{ 0, 0, 0, 0, 0, 0, 0, 0 },
				{ 0, 2, 2, 2, 2, 2, 2, 0 },
				{ 0, 2, 3, 3, 3, 3, 2, 0 },
				{ 0, 2, 3, 2, 2, 0, 2, 0 },
				{ 0, 2, 3, 2, 2, 0, 2, 0 },
				{ 0, 2, 0, 0, 0, 0, 2, 0 },
				{ 0, 2, 2, 2, 2, 2, 2, 0 },
				{ 0, 0, 0, 0, 0, 0, 0, 0 }
			};
		}

		public void InitCharset()
		{
			Char0 = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			Char1 = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 3, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			Char2 = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 3, 3, 3, 0, 0, 0, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 0, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 0, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			Char3 = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 0, 0, 0, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 0, 0, 0, 3 },
				{ 3, 3, 3, 3, 0, 0, 0, 3 },
				{ 3, 0, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			Char4 = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 0, 0, 3, 3 },
				{ 3, 0, 3, 3, 0, 0, 3, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 0, 0, 0, 0, 0, 0, 3 },
				{ 3, 3, 3, 3, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			Char5 = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 0, 0, 0, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			Char6 = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			Char7 = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 0, 3 },
				{ 3, 3, 3, 3, 3, 0, 0, 3 },
				{ 3, 3, 3, 3, 0, 0, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 3, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			Char8 = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			Char9 = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 3, 0, 0, 0, 0, 0, 3 },
				{ 3, 3, 3, 3, 0, 0, 0, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharA = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 0, 0, 0, 0, 0, 0, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharB = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharC = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharD = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 0, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharE = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 0, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 0, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharF = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 0, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharG = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 3, 0, 0, 0, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 3, 0, 0, 0, 0, 0, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharH = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 0, 0, 0, 0, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharI = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharJ = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 3, 0, 0, 0, 0, 3 },
				{ 3, 3, 3, 3, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 0, 0, 3, 3 },
				{ 3, 3, 0, 0, 0, 3, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharK = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 3, 0, 0, 3, 3 },
				{ 3, 0, 0, 0, 0, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 3, 3, 3 },
				{ 3, 0, 0, 3, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharL = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 0, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharM = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 3, 0, 0, 0, 3 },
				{ 3, 0, 0, 0, 0, 0, 0, 3 },
				{ 3, 0, 3, 0, 3, 0, 0, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharN = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 0, 3, 0, 0, 3 },
				{ 3, 0, 3, 0, 0, 0, 0, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharO = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharP = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharQ = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 0, 3 },
				{ 3, 0, 0, 3, 3, 3, 0, 3 },
				{ 3, 0, 0, 3, 0, 3, 0, 3 },
				{ 3, 0, 0, 3, 3, 0, 3, 3 },
				{ 3, 3, 0, 0, 0, 3, 0, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharR = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 0, 3, 3, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharS = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 0, 0, 0, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharT = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 0, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharU = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 0, 3, 3, 0, 0, 0, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharV = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 3, 0, 3, 0, 0, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharW = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 0, 3, 0, 3, 0, 0, 3 },
				{ 3, 0, 0, 0, 0, 0, 0, 3 },
				{ 3, 0, 0, 3, 0, 0, 0, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharX = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 3, 3, 3, 0, 0, 3 },
				{ 3, 3, 0, 3, 0, 0, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 3, 3, 3 },
				{ 3, 0, 0, 3, 3, 0, 3, 3 },
				{ 3, 0, 3, 3, 3, 3, 0, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharY = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 0, 0, 3, 3, 0, 0, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 3, 0, 0, 3, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharZ = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 0, 3 },
				{ 3, 3, 3, 3, 0, 0, 0, 3 },
				{ 3, 3, 3, 0, 0, 0, 3, 3 },
				{ 3, 3, 0, 0, 0, 3, 3, 3 },
				{ 3, 0, 0, 0, 3, 3, 3, 3 },
				{ 3, 0, 0, 0, 0, 0, 0, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharPer = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 0, 0, 3, 3, 3, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharDash = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 0, 0, 0, 0, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharMul = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 0, 3, 3, 3, 0, 3 },
				{ 3, 3, 3, 0, 3, 0, 3, 3 },
				{ 3, 3, 3, 3, 0, 3, 3, 3 },
				{ 3, 3, 3, 0, 3, 0, 3, 3 },
				{ 3, 3, 0, 3, 3, 3, 0, 3 },
				{ 3, 3, 3, 3, 3, 3, 3, 3 }
			};

			CharHeart = new int[,] {
				{ 3, 3, 3, 3, 3, 3, 3, 3 },
				{ 3, 3, 0, 0, 3, 0, 0, 3 },
				{ 3, 0, 3, 2, 0, 2, 2, 0 },
				{ 3, 0, 3, 2, 2, 2, 2, 0 },
				{ 3, 0, 2, 2, 2, 2, 2, 0 },
				{ 3, 3, 0, 2, 2, 2, 0, 3 },
				{ 3, 3, 3, 0, 2, 0, 3, 3 },
				{ 3, 3, 3, 3, 0, 3, 3, 3 }
			};
			#endregion
		}
	}

	public class SoundContainer
	{
		public MemoryStream musicMemStream, btMemStream, bpMemStream, lcMemStream, xMemStream;
		public WaveStream musicWavStream, btWavStream, bpWavStream, lcWavStream, xWavStream;
		public PlaybackStream musicLoopStream, btPlayback, bpPlayback, lcPlayback, xPlayback;
		public WaveOutEvent musicOut, btOut, bpOut, lcOut, xOut;

        public void PlayMusic(int mnr)
		{
			switch (mnr)
			{
				case 0: // Type A
					try { musicOut.Stop(); } catch (Exception ex) { Console.WriteLine(ex.ToString()); }
					musicMemStream = new MemoryStream(Resources.tetris_typeA_loop);
					musicWavStream = new Mp3FileReader(musicMemStream);
					musicLoopStream = new PlaybackStream(musicWavStream);
					musicOut = new WaveOutEvent() { Volume = 0.5F };
					musicOut.Init(musicLoopStream);
					musicOut.Play();
					break;
				case 1: // Type B
					try { musicOut.Stop(); } catch (Exception ex) { Console.WriteLine(ex.ToString()); }
					musicMemStream = new MemoryStream(Resources.tetris_typeB_loop);
					musicWavStream = new Mp3FileReader(musicMemStream);
					musicLoopStream = new PlaybackStream(musicWavStream);
					musicOut = new WaveOutEvent() { Volume = 0.5F };
					musicOut.Init(musicLoopStream);
					musicOut.Play();
					break;
				case 2: // Type C
					try { musicOut.Stop(); } catch (Exception ex) { Console.WriteLine(ex.ToString()); }
					musicMemStream = new MemoryStream(Resources.tetris_typeC_loop);
					musicWavStream = new Mp3FileReader(musicMemStream);
					musicLoopStream = new PlaybackStream(musicWavStream);
					musicOut = new WaveOutEvent() { Volume = 0.5F };
					musicOut.Init(musicLoopStream);
					musicOut.Play();
					break;
				case 3: // Main menu theme
					try { musicOut.Stop(); } catch (Exception ex) { Console.WriteLine(ex.ToString()); }
					musicMemStream = new MemoryStream(Resources.tetris_typeC_loop);
					musicWavStream = new Mp3FileReader(musicMemStream);
					musicLoopStream = new PlaybackStream(musicWavStream) { EnableLooping = false };
					musicOut = new WaveOutEvent() { Volume = 0.5F };
					musicOut.Init(musicLoopStream);
					musicOut.Play();
					break;
				case 4: // High score entry screen theme
					try { musicOut.Stop(); } catch (Exception ex) { Console.WriteLine(ex.ToString()); }
					musicMemStream = new MemoryStream(Resources.tetris_score_loop);
					musicWavStream = new Mp3FileReader(musicMemStream);
					musicLoopStream = new PlaybackStream(musicWavStream) { LoopPosition = 230400 };
					musicOut = new WaveOutEvent() { Volume = 0.5F };
					musicOut.Init(musicLoopStream);
					musicOut.Play();
					break;

			}
		}

		public void StopMusic()
		{
			musicOut.Stop();
			musicMemStream.Dispose();
			musicWavStream.Dispose();
			musicLoopStream.Dispose();
			musicOut.Dispose();
			try { musicOut.Stop(); } catch (Exception ex) { Console.WriteLine(ex.ToString()); } // For StartThemeLoop
		}

		// This method has been disabled because it was causing exceptions. Something to do with threading.
		public void PlaySound(int snr)
		{
			/*switch (snr)
			{
				case 0: // block turn
					btMemStream = new MemoryStream(Resources.tetris_blockturn);
					btWavStream = new Mp3FileReader(btMemStream);
					btPlayback = new PlaybackStream(btWavStream) { EnableLooping = false };
					btOut = new WaveOutEvent() { Volume = 0.5F };
					btOut.Init(btPlayback);
					btOut.Play();
					break;
				case 1: // block place
					bpMemStream = new MemoryStream(Resources.tetris_blockplaced);
					bpWavStream = new Mp3FileReader(bpMemStream);
                    bpPlayback = new PlaybackStream(bpWavStream) { EnableLooping = false };
					bpOut = new WaveOutEvent() { Volume = 0.5F };
					bpOut.Init(bpPlayback);
					bpOut.Play();
					break;
				case 2: // line clear
					lcMemStream = new MemoryStream(Resources.tetris_lineclear);
					lcWavStream = new Mp3FileReader(lcMemStream);
					lcPlayback = new PlaybackStream(lcWavStream) { EnableLooping = false };
					lcOut = new WaveOutEvent() { Volume = 0.5F };
					lcOut.Init(lcPlayback);
					lcOut.Play();
					break;
                case 3: // blip on x block movement
                    xMemStream = new MemoryStream(Resources.tetris_xblip);
                    xWavStream = new Mp3FileReader(xMemStream);
                    xPlayback = new PlaybackStream(xWavStream) { EnableLooping = false };
                    xOut = new WaveOutEvent() { Volume = 0.5F };
                    xOut.Init(xPlayback);
                    xOut.Play();
                    break;
			}*/
		}
	}

	public class PlaybackStream : WaveStream
	{
		WaveStream sourceStream;
        long loopPosition;

		public PlaybackStream(WaveStream sourceStream)
		{
			this.sourceStream = sourceStream;
			EnableLooping = true; // default value
            loopPosition = 0;     // default value
		}

		public bool EnableLooping { get; set; }

		public override WaveFormat WaveFormat
		{
			get { return sourceStream.WaveFormat; }
		}

		public override long Length
		{
			get { return sourceStream.Length; }
		}

		public override long Position
		{
			get { return sourceStream.Position; }
			set { sourceStream.Position = value; }
		}

        public long LoopPosition
        {
            get { return loopPosition; }
            set { loopPosition = value; }
        }

		public override int Read(byte[] buffer, int offset, int count)
		{
			int totalBytesRead = 0;
			while (totalBytesRead < count)
				{
					int bytesRead = sourceStream.Read(buffer, offset + totalBytesRead, count - totalBytesRead);
					if (bytesRead == 0)
					{
						if (sourceStream.Position == 0 || !EnableLooping)
						{
							// something wrong with the source stream or no loop
							break;
						}
						// restart the stream
						sourceStream.Position = loopPosition;
					}
					totalBytesRead += bytesRead;
				}
			return totalBytesRead;
		}
	}
}
